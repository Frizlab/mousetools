# MouseTools
Take control of the mouse on macOS from the command-line tool.

## Disclaimer
I am **not** the original author of this project. The original project is found here:
https://www.hamsoftengineering.com/codeSharing/MouseTools/MouseTools.html

## More or Less the Original Readme of the Project (from the Webpage)

This command line tool will help you perform tasks with your mouse.  
**Note**: By default screen coordinates are measured from the top-left corner of the screen but with the `[-b]` switch they can be measured from the bottom-left.

```
SWITCHES:
[-h] return this help text
[-b] coordinates are measured from bottom-left corner of the screen
[-location] return the current mouse location
[-x "xValue" -y "yValue"] move the mouse to the {xValue, yValue} location
[-mouseSteps numSteps] move mouse in number-of-steps to the location
[-leftClick] perform a mouse left-click at the current mouse location
[-doubleLeftClick] perform a mouse double-click with the left mouse button
[-rightClick] perform a mouse right-click at the current mouse location
[-shiftKey] shift key down, useful when performing a left-click event
[-commandKey] command key down, useful when performing a left-click event
[-optionKey] option key down, useful when performing a left-click event
[-controlKey] control key down, useful when performing a left-click event
[-leftClickNoRelease] perform a mouse click and do not release the mouse click
[-releaseMouse] release the mouse after using -leftClickNoRelease

EXAMPLES:
1. get mouse location (measured from top-left)
MouseTools -location

2. get mouse location (measured from bottom-left)
MouseTools -b -location

3. move the mouse to a screen location
MouseTools -x xValue -y yValue

4. move the mouse in 1000 steps to a screen location
MouseTools -x xValue -y yValue -mouseSteps 1000

5. right-click the mouse at the current mouse position
MouseTools -rightClick

6. move the mouse to the given coordinates and perform a left-click
MouseTools -x xValue -y yValue -leftClick

7. move the mouse to the given coordinates and perform a shift-click
MouseTools -x xValue -y yValue -leftClick -shiftKey
```

### Example usages of the tool with AppleScript
[Open this Script in your Editor](applescript://com.apple.scripteditor?action=new&script=--%20get%20the%20current%20mouse%20coordinates%20(measured%20from%20top-left%20corner%20of%20screen)%0Aset%20mouseToolsPath%20to%20(path%20to%20home%20folder%20as%20text)%20&%20%22UnixBins:MouseTools%22%0A%0Adelay%203%20--%203%20seconds%20after%20starting%20the%20script%20to%20move%20the%20mouse%20where%20you%20want%0Aset%20%7Bx,%20y%7D%20to%20paragraphs%20of%20(do%20shell%20script%20quoted%20form%20of%20POSIX%20path%20of%20mouseToolsPath%20&%20%22%20-location%22)) 
```applescript
-- get the current mouse coordinates (measured from top-left corner of screen)
set mouseToolsPath to (path to home folder as text) & "UnixBins:MouseTools"

delay 3 -- 3 seconds after starting the script to move the mouse where you want
set {x, y} to paragraphs of (do shell script quoted form of POSIX path of mouseToolsPath & " -location")
```

[Open this Script in your Editor](applescript://com.apple.scripteditor?action=new&script=--%20move%20the%20mouse%20to%20the%20x/y%20coordinates%20(as%20measured%20from%20the%20top-left%20part%20of%20the%20screen)%20and%20perform%20a%20mouse%20left-click%0Aset%20mouseToolsPath%20to%20(path%20to%20home%20folder%20as%20text)%20&%20%22UnixBins:MouseTools%22%0Aset%20x%20to%20164%0Aset%20y%20to%20114%0A%0Ado%20shell%20script%20quoted%20form%20of%20POSIX%20path%20of%20mouseToolsPath%20&%20%22%20-x%20%22%20&%20(x%20as%20text)%20&%20%22%20-y%20%22%20&%20(y%20as%20text)%20&%20%22%20-leftClick%22) 
```applescript
-- move the mouse to the x/y coordinates (as measured from the top-left part of the screen) and perform a mouse left-click
set mouseToolsPath to (path to home folder as text) & "UnixBins:MouseTools"
set x to 164
set y to 114

do shell script quoted form of POSIX path of mouseToolsPath & " -x " & (x as text) & " -y " & (y as text) & " -leftClick"
```

[Open this Script in your Editor](applescript://com.apple.scripteditor?action=new&script=--%20move%20the%20mouse%20to%20the%20x/y%20coordinates%20in%201000%20steps%0Aset%20mouseToolsPath%20to%20(path%20to%20home%20folder%20as%20text)%20&%20%22UnixBins:MouseTools%22%0Aset%20x%20to%20164%0Aset%20y%20to%20114%0A%0Ado%20shell%20script%20quoted%20form%20of%20POSIX%20path%20of%20mouseToolsPath%20&%20%22%20-x%20%22%20&%20(x%20as%20text)%20&%20%22%20-y%20%22%20&%20(y%20as%20text)%20&%20%22%20-mouseSteps%201000%22) 
```applescript
-- move the mouse to the x/y coordinates in 1000 steps
set mouseToolsPath to (path to home folder as text) & "UnixBins:MouseTools"
set x to 164
set y to 114

do shell script quoted form of POSIX path of mouseToolsPath & " -x " & (x as text) & " -y " & (y as text) & " -mouseSteps 1000"
```
